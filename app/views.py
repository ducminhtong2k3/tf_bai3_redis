from app import app
from app import r, q
from app.tasks import count_words
from flask import render_template, request


@app.route('/')
def index():
    return "hello"


@app.route("/add-task", methods=['POST', 'GET'])
def add_task():

    jobs = q.jobs
    message = None

    if request.args:

        url = request.args.get("url")

        task = q.enqueue(count_words, url)

        jobs = q.jobs

        q_len = len(q)

        message = f"Task queued at {task.enqueued_at.strftime('%a %d %b %Y %H:%M:%S')}. {q_len} jobs queued"

    return render_template("add_task.html", message=message, jobs=jobs)
